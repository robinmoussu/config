# Qt
set -gx PATH "/grid/common_pkgs/qt/v5.15.2/bin:$PATH"

set -gx PATH "/grid/common_pkgs/gperf/v3.1/bin/:$PATH"
set -gx PATH "/grid/common/pkgs/gcc/v8.3.0/bin:$PATH"
set -gx PATH "/grid/common_pkgs/gdb/v10.1/bin:$PATH"

if status is-interactive
    # Commands to run in interactive sessions can go here

    # set -gx PATH "/grid/common_pkgs/gperf/v3.1/bin/:$PATH"
    # set -gx PATH "/grid/common/pkgs/gcc/v8.3.0/bin:$PATH"
    # set -gx PATH "/grid/common_pkgs/gdb/v8.3/bin:$PATH"

    set -gx PATH "/grid/common/pkgs/ninja/1.9.0/:$PATH"
    # set -gx PATH "/grid/common_pkgs/cmake/3.24.2/bin/:$PATH"
    # set -gx PATH "~/Works/:$PATH # for edxbuild"

    set -gx LD_LIBRARY_PATH "/grid/common/pkgs/gcc/v8.5.0/lib64:/grid/common/pkgs/gcc/v8.5.0:/grid/common_pkgs/icu/v65/lib/:/grid/common/pkgs/gperftools/v2.5/lib/:output/distribution/edxact/lib/linux_x86_64/:/grid/common/pkgs/qt/v5.15.2_opensource/lib/:output/distribution/edxact/oa_v22.50.089/lib/linux_rhel70_gcc85x_64/opt/:$LD_LIBRARY_PATH"

    set -gx PATH "$HOME/.cargo/bin:$PATH"
    set -gx PATH "$HOME/.local/bin:$PATH"
    set -gx HELIX_RUNTIME "$HOME/dev/helix/runtime"

    # set -gx CMAKE_C_COMPILER   "/grid/common/pkgs/gcc/v8.5.0/bin/gcc"
    # set -gx CMAKE_CXX_COMPILER "/grid/common/pkgs/gcc/v8.5.0/bin/g++"
    # set -gx PATH  "/grid/common/pkgs/gcc/v8.5.0/bin/:$PATH"
    # set -gx CMAKE_CXX_COMPILER "/grid/common/pkgs/gcc/v8.5.0/bin/g++"
    # 
    # # set -gx EDITOR nvim
    # set -gx EDITOR hx
end

;; ANSI sub-layer (let kanata ignore all other keys)
(defsrc
  esc ret bspc
  q        w        e        r        t        y        u        i        o        p
  a        s        d        f        g        h        j        k        l        ;
  z        x        c        v        b        n        m        ,        .        /
                 lalt     lmeta           spc        rmeta       ralt
)

;; Base layer (active by default when kanata starts up):
;;  - home-row mods
;;  - the 3 main thumb keys become mod/taps
(deflayer base-layer
  XX XX XX
  _        _        _        _        _        _        _        _        _        _
  @a-num   @s-lmeta @d-lalt  @f-lctl  _        _        @j-rctl  @k-lalt  @l-rmeta @semi-num
  _        _        _        _        _        _        _        _        _        _
               @bspc-sft  @bspc-sft      @spc        @ret-altgr  @ret-altgr
)

(deflayer nav-num
  XX XX XX
  esc      home     up       end      pgup     @quote   kp7      kp8      kp9      esc
  @tab-num @lft-lm  @down-la @rght-lc pgdn     @comma   @kp4-rc  @kp5-la  @kp6-rm  @tab-num
  esc      _        _        _        _        kp.      kp1      kp2      kp3      esc
                    _        _         @spc-hexa     @kp0-altgr  @kp0-altgr
)

(deflayer hexa
  XX XX XX
   _        _        _        _        _        _         _        _        _        _
   _        _        _        _        _        _         @d       @e       @f       _
   _        _        _        _        _        _         @a       @b       @c       _
                     _        _            _              _        _
)

(deflayer swap-fn
  XX XX XX
  p        o        i        u        y        _        f7       f8       f9       f12
  ;        l        k        j        h        lrld     f4       f5       f6       f11
  /        .        ,        m        n        _        f1       f2       f3       f10
                 @del-sft @del-sft        _             esc      esc
)

;; ;; Symbol layer (= AltGr except for the left thumb key)
;; (deflayer r-symbols
;;   RA-q     RA-w     RA-e     RA-r     RA-t     RA-y     RA-u     RA-i     RA-o     RA-p
;;   RA-a     RA-s     RA-d     RA-f     RA-g     RA-h     RA-j     RA-k     RA-l     RA-;
;;   RA-z     RA-x     RA-c     RA-v     RA-b     RA-n     RA-m     RA-,     RA-.     RA-/
;;                            del          -           _
;; )

;; Special key aliases
(defalias
  ;; Space-cadet thumb keys: BackSpace, Space and Return
  ;;  - acts as a modifier by default, or as BS/Ret when tapped separately;
  ;;  - works great with keyboard layouts where neither Shift nor AltGr are used outside of the 3×10
  ;; main alpha keys.
  bspc-sft  (tap-hold-press 200 200 bspc lsft)
  spc       (tap-hold-press 200 200 spc  (layer-toggle swap-fn))
  ret-altgr (tap-hold-press 200 200 ret  ralt)
  del-sft   (tap-hold-press 200 200 del lsft)

  ;; Home-row mods
  ;; Must be hold long enough (number of ms on the right) to become a modifier.
  a-num    (tap-hold 000 200 a (layer-toggle nav-num))
  s-lmeta  (tap-hold 200 200 s lmeta)
  d-lalt   (tap-hold 250 250 d lalt)
  f-lctl   (tap-hold 200 200 f lctrl)
  j-rctl   (tap-hold 200 200 j rctrl)
  k-lalt   (tap-hold 250 250 k lalt)
  l-rmeta  (tap-hold 200 200 l rmeta)
  semi-num (tap-hold 000 200 ; (layer-toggle nav-num))

  ;; Num layer home-row mod
  tab-num (tap-hold 200 200 tab (layer-toggle nav-num))
  lft-lm  (tap-hold 200 200 lft lmeta)
  down-la (tap-hold 200 200 down lalt)
  rght-lc (tap-hold 200 200 rght lctrl)
  kp4-rc  (tap-hold 200 200 kp4 rctrl)
  kp5-la  (tap-hold 200 200 kp5 lalt)
  kp6-rm  (tap-hold 200 200 kp6 rmeta)
  esc-num (tap-hold 200 200 esc (layer-toggle nav-num))
  kp0-altgr (tap-hold-press 200 200 kp0 (multi ralt (layer-toggle base-layer)))
  spc-hexa  (tap-hold-press 200 200 spc  (layer-toggle hexa))

  ;; numeric separator and quick acces to hexa digits
  ;; quote (unicode ’)
  ;; comma (unicode ,)
  ;; a (unicode a)
  ;; b (unicode b)
  ;; c (unicode c)
  ;; d (unicode d)
  ;; e (unicode e)
  ;; f (unicode f)
  ;; FIXME: currently there is no unicode support for macos
  ;; For the moment I use layout dependant keycode
  quote (multi w spc)
  comma g
  a a
  b q
  c h
  d i
  e f
  f /

  ;; Mouse wheel emulation
  mwu (mwheel-up    50 120)
  mwd (mwheel-down  50 120)
  mwl (mwheel-left  50 120)
  mwr (mwheel-right 50 120)
)

;; ;; Unused, I’m testing swap-fn currently (swap only on the left, fn on the right)
;; (deflayer fn
;;   _        _        _        _        _        _        f7       f8       f9       f12
;;   _        _        _        _        _        _        f4       f5       f6       f11
;;   _        _        _        _        _        _        f1       f2       f3       f10
;;                     _        _            _              _        _
;; )

;; ;; Unused, I’m testing swap-fn currently (swap only on the left, fn on the right)
;; (deflayer swap
;;   p        o        i        u        y        t         r        e        w        q
;;   ;        l        k        j        h        g         f        d        s        a
;;   /        .        ,        m        n        b         v        c        x        z
;;                     _        _            _              _        _
;; )

;; vim: set ft=lisp
